import sqlite3

conn = sqlite3.connect("projeckt.db") # соединение с локальной базой данных
crs = conn.cursor()

#create
def create_diplom():
	crs.execute("PRAGMA foreign_keys = ON")
	crs.execute(""" CREATE TABLE diplom(
						id INTEGER PRIMARY KEY,
						topic text NOT NULL,
						full_name text NOT NULL,
						supe text NOT NULL,
						ocs INTEGER,
						FOREIGN KEY (supe) REFERENCES supervisor(full_name))""")
	conn.commit()

def create_supervisor():
	crs.execute("PRAGMA foreign_keys = ON")
	crs.execute("""CREATE TABLE supervisor(
						_person_id INTEGER PRIMARY KEYT,
						accounting_degree text NOT NULL,
						full_name text NOT NULL,
						position text NOT NULL,
						the_department text
						)""")
	conn.commit()

#insert
def insert_diplom(ids, topic, full_name, supe, ocs):
	crs.execute("""INSERT INTO diplom VALUES (?, ?, ?, ?, ?);""", (
																ids,
																topic,
																full_name,
																supe,
																ocs))
	conn.commit()

def insert_supervisor(full_name, accounting_degree, position, the_department):
	crs.execute("""INSERT INTO supervisor(	full_name,
											accounting_degree,
										  	position,
										  	the_department) 
					VALUES (?, ?, ?, ?);""",(
											full_name,
											accounting_degree,
											position,
											the_department))
	conn.commit()

#update
def update_diplom(idz, what_up, to_up):
	crs.execute("""
		UPDATE diplom
		SET ? = '?'
		WHERE id = ?""", (what_up, to_up, idz))
	conn.commit()


def update_supervisor(idz, what_up, to_up):
	crs.execute("""
		UPDATE supervisor
		SET ? = '?'
		WHERE id = ?""", (what_up, to_up, idz))
	conn.commit()

#del 
def del_diplom(idz):
	crs.execute("""
		DELETE FROM diplom
		WHERE id = ?""", (idz))
	conn.commit()

def del_supervisor():
	crs.execute("""
		DELETE FROM supervisor
		WHERE id = ?""", (idz))
	conn.commit()

#select
def select_diplom(column, usl):
	if column == "id":
		crs.execute("""
			SELECT * FROM diplom
			WHERE id LIKE ? """, [(usl)])

	elif column == "topic":
		crs.execute("""
			SELECT * FROM diplom
			WHERE topic LIKE ? """, [(usl)])

	elif column == "full_name":
		crs.execute("""
			SELECT * FROM diplom
			WHERE full_name LIKE ? """, [(usl)])

	elif column == "supe":
		crs.execute("""
			SELECT * FROM diplom
			WHERE supe LIKE ? """, [(usl)])

	elif column == "ocs":
		crs.execute("""
			SELECT * FROM diplom
			WHERE ocs LIKE ? """, [(usl)])

	print(crs.fetchall())
	conn.commit()

def select_supervisor(column, usl):
	if column == "accounting_degree":
		crs.execute("""
			SELECT * FROM supervisor
			WHERE accounting_degree LIKE ? """, [(usl)])

	elif column == "position":
		crs.execute("""
			SELECT * FROM supervisor
			WHERE position LIKE ? """, [(usl)])
	elif column == "_person_id":
		crs.execute("""
			SELECT * FROM supervisor
			WHERE _person_id LIKE ? """, [(usl)])
	elif column == "the_department":
		crs.execute("""
			SELECT * FROM supervisor
			WHERE the_department LIKE ? """, [(usl)])

	elif column == "full_name":
		crs.execute("""
			SELECT * FROM supervisor
			WHERE full_name LIKE ? """, [(usl)])

	conn.commit()
	return crs.fetchall()
	
def printall():
	crs.execute("""SELECT * FROM supervisor""")
	crs.execute("""SELECT * FROM diplom""")
	conn.commit()
	return crs.fetchall()